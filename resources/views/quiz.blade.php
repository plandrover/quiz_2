<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="{{ asset('css/quiz.css') }}" rel="stylesheet">
    {{--<link rel="stylesheet" type="text/css" href="/css/style.c
<body>
{{--<form action="http://example.com/handle-form">--}}
    {{--Age<br>--}}
    {{--<input type="number" name="age" value=""><br>--}}
    {{--Name:<br>--}}
    {{--<input type="text" name="name" value=""><br><br>--}}
    {{--<input type="submit" value="Submit">--}}
    {{--</form>--}}
    {{--<h1 class="pod">dqbdqdb</h1>--}}
    {{--<p class="token key">debdwdiod</p>--}}
    {{--<p >debdwdiod</p>--}}
    {{--<p>debdwdiod</p>--}}
    {{--<p class="token key">debdwdiod</p>--}}
    {{--<p class="token">debdwdiod</p>--}}
    {{--<p>debdwdiod</p>--}}
    {{--<p class="key">debdwdiod</p>--}}

    {{--<h3 class="token key">niodnioqn</h3>--}}

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7 colo top">
                /
            </div>
            <div class="col-lg-5 colo top">
                /
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="row">
                    <div class="col-lg-6 colo lil-left">/</div>
                    <div class="col-lg-6 colo lil-left">/</div>
                </div>
                <div class="row">
                    <div class="col-lg-6 colo lil-left">/</div>
                    <div class="col-lg-6 colo lil-left">/</div>
                </div>
            </div>
            <div class="col-lg-8 colo big-left">/</div>
        </div>

    </div>

    </body>
</html>